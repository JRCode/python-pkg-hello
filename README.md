# hellojr

A sample Python project for demonstration purposes that contains automated unit tests, code coverage, code quality, and package publishing.

[![pipeline status](https://gitlab.com/JRCode/python-pkg-hello/badges/master/pipeline.svg)](https://gitlab.com/JRCode/python-pkg-hello/commits/master)
[![coverage report](https://gitlab.com/JRCode/python-pkg-hello/badges/master/coverage.svg)](https://gitlab.com/JRCode/python-pkg-hello/commits/master)
[![readthedocs](https://readthedocs.org/projects/python-pkg-hello/badge/?version=latest)](https://python-pkg-hello.readthedocs.io/en/latest/)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/hellojr)](https://pypi.org/project/hellojr/)
[![PyPI](https://img.shields.io/pypi/v/hellojr)](https://pypi.org/project/hellojr/)
