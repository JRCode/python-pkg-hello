=============
Documentation
=============

We use **sphinx** to generate both our API and user level documentation. This is what you are reading now.

------------------
User Documentation
------------------

We use the **sphinx** tools and libraries to generate our documentation.

You set up documentation with the command: ``sphinx-quickstart``. This will guide you through creating your documentation.

After that you will need to read up on **sphinx** and the **reStructureText** file format.

Here are some useful links:

* https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html
* http://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html#

You can build the user and API documentation by running the following:

.. code-block ::

  sphinx-apidoc -o docs/source --ext-autodoc -f -a hellojr *_tests.py
  cd docs
  make html

This is not actually how the end result is deployed as our documentation is automatically built by **readthedocs.io** against our master branch. Thus everytime we successfully merge into master
**readthedocs.io** will rebuild our documentation.

We still include the building of documentation in our **gitlab-ci.yml** file so that we know the build on **readthedocs.io** will be successful.
