============
Code Quality
============

In this demonstration we use the **flake8** library to perform code quality checks. **Flake8** has some nice features so we
can decide what checks are important to us. It's mostly an enforcer of the PEP-8 standard.

For example we set the default 79 character line limit to 100. This is a personal preference.

You can initiate the code quality checks (lint) by running the following:

``flake8``

-------------
Configuration
-------------

Code quality checks are configured in **setup.cfg** under **[flake8]**.