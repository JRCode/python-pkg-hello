hellojr package
===============

Submodules
----------

hellojr.addition module
-----------------------

.. automodule:: hellojr.addition
   :members:
   :undoc-members:
   :show-inheritance:

hellojr.command module
----------------------

.. automodule:: hellojr.command
   :members:
   :undoc-members:
   :show-inheritance:

hellojr.localization module
---------------------------

.. automodule:: hellojr.localization
   :members:
   :undoc-members:
   :show-inheritance:

hellojr.multiplication module
-----------------------------

.. automodule:: hellojr.multiplication
   :members:
   :undoc-members:
   :show-inheritance:

hellojr.web module
------------------

.. automodule:: hellojr.web
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: hellojr
   :members:
   :undoc-members:
   :show-inheritance:
