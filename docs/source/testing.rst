=======
Testing
=======

In this demonstration we use the standard **unittest** library and the **nosetest** runner. Nosetest is a command line tool
and library that has a lot o useful features for running tests.

You can run the automated test suite and code coverage by running the following:

``nosetests``

---------
Discovery
---------

**Nose** will automatically find tests. This could be anything in a **tests/** directory, or files that end with **_tests.py**.

This is configurable in **Nose**; however the approach I prefer is to have each Python file have a corresponding **_tests.py** file.

I have these located in the same directory as this allows the tests and the code to live side-by-side. It also allows you to quickly
identify if you have tests for a particular Python file.

This is very much a personal choice and you are free to manage this however you want.

-------------
Code Coverage
-------------

Code coverage is provided by the **coverage** Python package. **Nose** happens to integrate with this and allows us to detect how
much of our code is actually covered by tests.

-------------
Configuration
-------------

Testing and coverage are configured in **setup.cfg** under **[nosetests]**.
