hellojr
===================================

A sample Python project for demonstration purposes that contains automated unit tests, code coverage, code quality, localization and package publishing.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   quality
   testing
   localization
   documentation
   packaging
   
.. toctree::
   :maxdepth: 2
   :caption: API:
   
   hellojr
 
  
Indices and tables
==================
 
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`