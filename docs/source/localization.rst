============
Localization
============

In this demonstration we use **gettext** to manage localization. We wont go into the full scope of **gettext**
but this should be enough information to get you started. For more information I recommend the following:

* http://babel.pocoo.org/en/latest/intro.html
* http://babel.pocoo.org/en/latest/setup.html
* https://www.gnu.org/software/gettext/manual/html_node/Introduction.html
* https://docs.python.org/2/library/gettext.html#module-gettext

Localization has been pre-configured in **setup.cfg** to make management easier.

The process of localization is the following:

 * Wrap translated text in your Python code with:  ``_(TEXT)``.
 * Extract all of these strings from the source code and create a special **.pot** file.
 * Create catalogs for the languages you want to translate (ex: en, fr, ch).
 * Edit the catalog **.po** files and put in the translated text.
 * Compile the catalog from a text **.po** file into a binary **.mo** file

-------------------
Extracting Messages
-------------------

The first step in managing localization is to wrap your strings in a special ``_()`` method. This is the typical way in Python
that translated strings are "marked". Marking a string allows the translation engine to actually translate the text into the active
locale.

Before we can build any special translation files, we need to extract the messages from the source code and build a special **hellojr.pot** file.

To extract messages you run:

``python setup.py extract_messages``

This will update **locale/hellojr.pot** which is the template file used to create the locales.

^^^^^^^^^^^^^
Configuration
^^^^^^^^^^^^^

This command is configured in **setup.cfg** under **[extract_messages]**.


---------------------
Initializing Catalogs
---------------------

Once you have extracted the messages, you will need to apply the **hellojr.pot** file to your desired locales.

A locale in **gettext** is referred to as a catalog. The **fr** and **en** catalogs have been provided.

To create a new catalog (add a new locale) run the following:

``python setup.py init_catalog -l LOCALE``

^^^^^^^^^^^^^
Configuration
^^^^^^^^^^^^^

This command is configured in **setup.cfg** under **[init_catalog]**.

-----------------
Updating Catalogs
-----------------

If you make changes to the **hellojr.pot** file (for example you have marked new strings for translations). You will need
to update the corresponding catalogs. **gettext** can manage this for you.

You can update all the catalogs by running the following:

``python setup.py update_catalog``

^^^^^^^^^^^^^
Configuration
^^^^^^^^^^^^^

This command is configured in **setup.cfg** under **[update_catalog]**.

--------------------
Editing Translations
--------------------

Now that you can extract messages, create catalogs, and update them you need to actually add the translated text.

To edit the translations you can open up the locale you want to add strings for. You will find these located in **locale/LOCALE/LC_MESSAGES/hellojr.po**.

The **msgid** represents the entire string you marked in the Python code. The **msgstr** field represents the end result (the translated message).

Update the **msgstr** for all the translations (old translations will be preserved). Then you can move onto the compilation step.

------------------
Compiling Catalogs
------------------

Once all the translation **.po** files have been created you need to compile them into a format that **gettext** will use. These are **.mo** files which are binary
versions of the **.po** files.

You can compile your catalogs by running the following:

``python setup.py compile_catalog``

You should now have **hellojr.mo** files in the **locale/LOCALE/LC_MESSAGES/** directory.

It is not recommended to commit the **.mo** files to source code as the **.po** files are all that's required for building (dont put build assets in your repository).


^^^^^^^^^^^^^
Configuration
^^^^^^^^^^^^^

This command is configured in **setup.cfg** under **[compile_catalog]**.

