============
Packaging
============

In this demonstration we use standard Python packaging. This project is distributed a source tarball and as a Python wheel.

--------------------
Creating the Package
--------------------

To create the package you can run the following:

``python setup.py sdist bdist_wheel``

This will create a source tarball and a Python wheel in the **dist** directory.

-----------
Credentials
-----------

In order to upload packages to PyPI you will need an account.

You should also configure an API token (on PyPI) so you dont have to provide your account credentials.

Once you have done this you will be able to use the API token in your continuous integration to have
successful builds automatically uploaded to PyPI.

You can see how this is done in the **gitlab-ci.yml** file in the repository.

-----------------------------
Uploading the Package to PyPI
-----------------------------

To upload the package you can run the following:

``twine upload -u __token__ -p $PYPI_API_TOKEN dist/*``

This command will automatically upload the package to PyPi