"""
Multiplication methods.
"""


def multiply(a: int, b: int) -> int:
    """
    Multiply two numbers together and return the result.
    """
    return a * b
