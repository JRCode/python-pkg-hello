"""
Addition methods.
"""


def add(a: int, b: int) -> int:
    """
    Add two integers together and return the result.
    """
    return a + b
